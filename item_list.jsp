<!--html한글깨짐방지/JSP 한글깨짐방지/JSP에 필요한 import-->
<meta http-equiv="content-Type" content="text/html; charset=utf-8" />
<%@ page contentType="text/html; charset=utf-8" %>
<%@ page import = "java.sql.*,javax.sql.*,java.io.*,java.util.*,java.text.*" %>
<!--java.sql.* : sql문을 사용하기 위해-->

<html>
<head>
<title>KSW01_재고관리</title>
<!-- mystyle.css파일 import하여 적용-->
<link rel="stylesheet" type="text/css" href="mystyle.css">

<!--전역변수 선언-->
<%! int fromPT; %>
<%! int cntPT; %>
<%! int pages; %>
<%! int nowpg; %>
<%! int spg; %>
<%! int epg; %>
<%! int rows; %>

</head>
<body>
<center>	<!--가운데 정렬-->
<table id="top" cellspacing="0"> <!--테이블 생성-->
	<tr>
		<td><h1>(주)트와이스 재고 현황 - 전체현황</h1></td>
	</tr>
</table>
<%	// JDBC드라이버 로드
	Class.forName("com.mysql.jdbc.Driver");		
	//Connection 객체 생성:DB연결
	Connection conn = DriverManager.getConnection("jdbc:mysql://localhost/kopoctc", "root", "1234"); 
	// statement 객체 생성
	Statement stmt = conn.createStatement();	
	// statement2 객체 생성
	Statement stmt2 = conn.createStatement();	
	// item테이블의 전체 데이터를 오름차순으로 조회
	ResultSet rset = stmt.executeQuery("select * from item order by id desc;");	
	// item테이블의 데이터 수 조회
	ResultSet rs = stmt2.executeQuery("select count(*) from item;");			
	rows=0;	// 변수선언 및 초기값 선언
	while (rs.next()){
	// true/false로 반환		
		rows = rs.getInt(1);	// 1번째 필드 값을 가져온다. (필드번호대신 필드명을 쓰기도 한다.-명료성)
	}
	
	///// 페이징을 위한 변수 선언과 조건
	//전체데이터 = rows
	//페이지당 시작번호
	String fromPTs = request.getParameter("fromPT");
	try{
		fromPT = Integer.parseInt(fromPTs);
	}catch (Exception e) {
		fromPT=1;	//fromPT의 값이 없을 경우 1로 시작
	}
	// 페이지당 출력할 게시물 수
	String cntPTs = request.getParameter("cntPT");
	try{
		cntPT = Integer.parseInt(cntPTs);
	}catch (Exception e) {
		cntPT=20;	//cntPT의 기본값
	}
	
	//페이지수 계산 = pages
	pages=rows/cntPT;
	if(rows%cntPT!=0){
		pages++;
	}
	//마지막 페이지의 게시글 시작번호
	int maxfromPT=(pages-1)*cntPT+1;
	//현재페이지
	nowpg=fromPT/cntPT+1;
	//이전페이지의 게시글 시작번호
	int beforefromPT;
	if(nowpg<=1){
		beforefromPT=1;
	}else{
		beforefromPT=(nowpg-2)*cntPT+1;
	}
	//다음페이지의 게시글 시작번호
	int nextfromPT;
	if(nowpg>=pages){
		nextfromPT=maxfromPT;
	}else{
		nextfromPT=(nowpg)*cntPT+1;
	}
	//////
%>

<table cellspacing=0 border=1>
<tr>
	<th width='10%'> 상품번호 </th>
	<th width='50%'> 상품명 </th>
	<th width='10%'> 현재 재고수 </th>
	<th width='10%'> 상품등록일 </th>
	<th width='10%'> 재고등록일 </th>
</tr>
<%	// 상품이 등록되어 있을 경우
	if(rows!=0){
		int cnt=0;	// 기준이 되는 번호 : cnt
		while (rset.next()) {
			cnt++;
			if(fromPT>cnt) {
			// fromPT까지는 화면 출력 no / fromPT 다음부터 화면 출력
				continue;
			}else if(cnt>(fromPT+cntPT)-1){
			// fromPT+cntPT-1까지 화면에 출력
				break;
			}
			%>
			<tr>
				<td><%=rset.getInt(1)%></td>
				<td><a href=item_view.jsp?id=<%=rset.getInt(1)%>><%=rset.getString(2)%></td>
				<td><%=rset.getInt(3)%></td>
				<td><%=rset.getDate(4)%></td>
				<td><%=rset.getDate(5)%></td>
			</tr>
			<%
		}
	}else{
	// 상품이 등록되어 있지 않을 경우
		%>
		<tr>
			<td colspan="6" align="center">상품목록 없음</td>
		</tr>
		<%
	}
	%>
	</table>
	<!--페이징 구간-->
	<table cellspacing="0">
	<tr>
		<td>
		<a id="page" href="item_list.jsp?fromPT=1&cntPT=<%=cntPT%>">&lt&lt</a>
		<a id="page" href="item_list.jsp?fromPT=<%=beforefromPT%>&cntPT=<%=cntPT%>">&lt</a>
		<%
			for(int i = 0; i<pages; i++){
				if(nowpg==i+1){	//현재페이지일 경우 괄호 표시
					out.println("<a id='page' style='background-color:CadetBlue; color:white;' href='item_list.jsp?fromPT="+(i*cntPT+1)+"&cntPT="+cntPT+"'>"+(i+1)+"</a>");
				}else{	// 그 외의 경우
					out.println("<a id='page' href='item_list.jsp?fromPT="+(i*cntPT+1)+"&cntPT="+cntPT+"'>"+(i+1)+"</a>");
				}
			}
		%>
		<a id="page" href="item_list.jsp?fromPT=<%=nextfromPT%>&cntPT=<%=cntPT%>">&gt</a>
		<a id="page" href="item_list.jsp?fromPT=<%=maxfromPT%>&cntPT=<%=cntPT%>">&gt&gt</a>
		</td>
	</tr>
	</table>
	<table cellspacing="0">
		<tr>
		<form action="item_one.jsp">
			<td width="25%"></td>
			<td width="50%">
			<select name="opt">	<!--select에 대한 이름 지정-->
				<option value="id">상품번호</option>
				<option value="name">상품명</option>
			</select>
			<input type="search" name="search"><input type="submit" value="search"></td>
			<td id="end" width="25%">
			<input type="button" value="목록" onclick="window.location.href='item_list.jsp'">
			<input type="button" value="신규등록" onclick="window.location.href='item_insert.jsp'">
			</td>
		</form>
		</tr>
	</table>
<%	// 닫기
	rs.close();
	rset.close();
	stmt2.close();
	stmt.close();
	conn.close();
%>
</body>
</center>
</html>