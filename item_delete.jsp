<meta http-equiv="content-Type" content="text/html; charset=utf-8" />
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import = "java.sql.*, javax.sql.*,java.io.*" %>
<%@ page import="java.util.*, java.text.*"  %>
<html>
<head>
<title>KSW01_재고관리</title>
<!-- mystyle.css파일 import하여 적용-->
<link rel="stylesheet" type="text/css" href="mystyle.css">
<%	// 한글 깨짐 방지
	request.setCharacterEncoding("UTF-8");
	// 값을 받기 위한 파라미터
	int id;
	String ids = request.getParameter("id");
	id=Integer.parseInt(ids);
	String name = request.getParameter("name");
	String path = request.getParameter("photo");
	
%>
</head>
<body>
<center>
<table id="top" cellspacing=0>
	<tr>
		<td><h1>(주)트와이스 재고 현황 - 상품삭제</h1></td>
	</tr>

<%	// 객체 생성
	Class.forName("com.mysql.jdbc.Driver");
	// DB연결
	Connection conn = DriverManager.getConnection("jdbc:mysql://localhost/kopoctc", "root", "1234");
	// 해당 id의 데이터를 지우기 위한 sql문
	Statement stmt = conn.createStatement();
	stmt.execute("delete from item where id="+id+";");
	%>
	
	<tr>
		<td align="center"><b>[<%=name%>]</b> 상품이 삭제되었습니다.</td>
	</tr>
	<tr>
		<td id="end"><input type="button" value="재고현황" onclick="window.location.href='item_list.jsp'"></td>
	</tr>
	
	<%	// 닫기
	stmt.close();
	conn.close();
	///// 사진 파일을 제거하기 위한 과정
	String opath="/var/lib/tomcat8/webapps/ROOT/";
	String rpath=opath+path;
	File file = new File(rpath);
    if( file.exists() ){
	// 사진이 존재하는 확인     
		if(file.delete()){
		     //out.println("파일삭제 성공");
        }else{
            //out.println("파일삭제 실패");
        }
    }else{
	// 사진이 존재하지 않을 경우
        //out.println("파일이 존재하지 않습니다.");
		//out.println(rpath);
    }
	
%>
</table>
</center>
</body>
</html>