<meta http-equiv="content-Type" content="text/html; charset=utf-8" />
<%@ page contentType="text/html; charset=utf-8" %>
<%@ page import = "java.sql.*,javax.sql.*,java.io.*,java.util.*,java.text.*" %>
<html>
<head>
<title>KSW01_재고관리</title>
<!-- mystyle.css파일 import하여 적용-->
<link rel="stylesheet" type="text/css" href="mystyle.css">
<!--전역변수 선언-->
<%! int id; %>
<%! String name; %>
<%	// 한글 깨짐 방지하기 위해
	request.setCharacterEncoding("UTF-8");
	// list.jsp에서 보낸 값을 받기 위한 파라미터
	String search = request.getParameter("search");
	String opt = request.getParameter("opt");	
%>
</head>
<body>
<center>
<%	// 객체생성
	Class.forName("com.mysql.jdbc.Driver");
	// DB연결
	Connection conn=DriverManager.getConnection("jdbc:mysql://localhost/kopoctc","root","1234");
	// stmt 검색한 결과를 조회하기 위한 sql문
	Statement stmt=conn.createStatement();
	// stmt2 검색한 결과가 있는지 유무를 판단하기 위한 sql문
	Statement stmt2=conn.createStatement();
	ResultSet rs=stmt.executeQuery("select * from item where "+opt+" = '" + search + "';");
	ResultSet rs2=stmt2.executeQuery("select count(*) from item where "+opt+" = '" + search + "';");
	int rows=0;
	while(rs2.next()){
		rows=rs2.getInt(1);
	}
	if(rows==0){
	// 검색 결과가 없을 경우
		%>	<!-- 존재하지 않다고 알림창 -->
		<script type="text/javascript">
			alert("검색결과 없습니다. (날짜형식:YYYY-MM-DD)");
			location.href="item_list.jsp";
		</script>
		<%
	}
	%>
	<table id="top" cellspacing=0>
		<tr>
			<td rowspan=2><h1>(주)트와이스 재고 현황 - 검색결과</h1></td>
		</tr>
	</table>
	<form>
	<table cellspacing=0 border=1>
	<tr>
		<th width='10%'> 상품번호 </th>
		<th width='50%'> 상품명 </th>
		<th width='10%'> 현재 재고수 </th>
		<th width='10%'> 재고 파악일 </th>
		<th width='10%'> 상품등록일 </th>
	</tr>
	<%
	
	while (rs.next()) {
	%>
		<tr>
			<td align=center><%=rs.getInt(1)%></td>
			<td><a href=item_view.jsp?id=<%=rs.getInt(1)%>><%=rs.getString(2)%></td>
			<td><%=rs.getInt(3)%></td>
			<td><%=rs.getDate(4)%></td>
			<td><%=rs.getDate(5)%></td>
			
		</tr>
	<%
	}
	%>
	</table>
	<table cellspacing="0" width="1000" border="0">
		<tr>
			<td width="25%"></td>
			<form action="item_one.jsp">
			<td width="50%">
			<select name="opt">
				<option value="id">상품번호</option>
				<option value="name">상품명</option>
			</select>
			<input type="search" name="search"><input type="submit" value="search">
			</td>			
			</form>
			<td id="end" width="25%">
			<input type="button" value="목록" onclick="window.location.href='item_list.jsp'">
			<input type="button" value="신규등록" onclick="window.location.href='item_insert.jsp'">
			</td>
		</tr>
	</table>
	<%	// 닫기
	rs.close();
	rs2.close();
	stmt2.close();
	stmt.close();
	conn.close();
%>
</center>
</body>
</html>