<meta http-equiv="content-Type" content="text/html; charset=utf-8" />
<%@page import="com.oreilly.servlet.multipart.DefaultFileRenamePolicy"%>
<%@page import="com.oreilly.servlet.MultipartRequest"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import = "java.sql.*, javax.sql.*,java.io.*" %>
<%@ page import="java.util.*, java.text.*"  %>
<html>
<head>
<%	// 한글깨짐 방지
	request.setCharacterEncoding("UTF-8");
	// 현재시간을 가져오기 위한
	TimeZone tz;
	java.util.Date date = new java.util.Date();
	DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
	tz = TimeZone.getTimeZone("Asia/Seoul"); df.setTimeZone(tz);  
%>
</head>
<body>
<%	
	// 실제로 서버에 저장되는 path
   	String path = request.getRealPath("/File");
    // 파일 사이즈 설정 : 10M
	int size = 5 * 1024 * 1024; 
	// DefaultFileRenamePolicy 처리는 중복된 이름이 존재할 경우 처리할 때
    // request, 파일저장경로, 용량, 인코딩타입, 중복파일명에 대한 정책
	
		MultipartRequest multi = new MultipartRequest(request, path, size, "UTF-8", new DefaultFileRenamePolicy());
		// 넘어오는 값을 받기 위한 파라미터들(공통)
		String info = multi.getParameter("info");
		int id;
		String ids = multi.getParameter("id"); 
		id=Integer.parseInt(ids);
		int quantity;
		String quantitys = multi.getParameter("quantity");
		quantity=Integer.parseInt(quantitys);
		
		if(info.equals("INSERT")){
		// 재고 추가일 경우
			// 넘어오는 값을 받기 위한 파라미터들(추가)
			String name = multi.getParameter("name");
			String comment = multi.getParameter("comment");
				
				String fileName = "";    // 업로드한 파일 이름
				String originalFileName = "";    //  서버에 중복된 파일 이름이 존재할 경우 처리하기 위해
				// cos.jar라이브러리 클래스를 가지고 실제 파일을 업로드하는 과정
						
				// 전송한 전체 파일이름들을 가져온다.
				Enumeration files = multi.getFileNames();
				String str = (String)files.nextElement();
				
				//파일명 중복이 발생했을 때 정책에 의해 뒤에 1,2,3 처럼 숫자가 붙어 고유 파일명을 생성한다.
				// 이때 생성된 이름을 FilesystemName이라고 하여 그 이름 정보를 가져온다. (중복 처리)
				fileName = multi.getFilesystemName(str);
			if(fileName==null){
			// 사진 파일이 첨부되지 않았을 경우
				// 객체 생성
				Class.forName("com.mysql.jdbc.Driver");
				// DB연결
				Connection conn = DriverManager.getConnection("jdbc:mysql://localhost/kopoctc", "root", "1234");
				// stmt 데이터 추가
				Statement stmt = conn.createStatement();
				try{
					stmt.execute("insert into item (id,name,quantity,stock,enroll,comment) values ("+id+",'"+name+"',"+quantity+",'"+df.format(date)+"','"+df.format(date)+"','"+comment+"');");	
					//out.println("without photo");
					%>	<!--상품추가 완료-->
					<script type="text/javascript">
					alert("상품이 추가되었습니다.");
					location.href="item_list.jsp";
					</script>
					<%
				}catch(Exception e){
					%>	<!--상품번호가 중복될 경우 알림-->
					<script type="text/javascript">
					alert("상품번호가 중복됩니다.");
					window.history.back();
					</script>
					<%
				}
			}else{
			//사진 파일이 첨부되었을 경우
				originalFileName = multi.getOriginalFileName(str);
				out.println("절대 경로 : " + path + "/"+fileName+"<br/>");
				String rpath;
				rpath="/File/"+fileName;	
				// 객체 생성
				Class.forName("com.mysql.jdbc.Driver");
				// DB추가
				Connection conn = DriverManager.getConnection("jdbc:mysql://localhost/kopoctc", "root", "1234");
				// stmt 데이터 추가
				Statement stmt = conn.createStatement();
				try{	
				stmt.execute("insert into item (id,name,quantity,stock,enroll,comment,path) values ("+id+",'"+name+"',"+quantity+",'"+df.format(date)+"','"+df.format(date)+"','"+comment+"','"+rpath+"');");
				%>	<!--상품추가 완료-->
				<script type="text/javascript">
				alert("상품이 추가되었습니다.");
				location.href="item_list.jsp";
				</script>
				<%
				}catch(Exception e){
					%>	<!--상품번호가 중복될 경우 알림-->
					<script type="text/javascript">
					alert("상품번호가 중복됩니다.");
					window.history.back();
					</script>
					<%
				}
				//out.println("with photo");
			}	
		%>
		<script type="text/javascript"> location.href="item_list.jsp"; </script>
		<%
		}else if(info.equals("UPDATE")){
		// 재고 수정일 경우
			//out.println("grade");
			// 객체 생성
			Class.forName("com.mysql.jdbc.Driver");
			// DB 연결
			Connection conn = DriverManager.getConnection("jdbc:mysql://localhost/kopoctc", "root", "1234");
			//  stmt 해당 id의 데이터 수정
			Statement stmt = conn.createStatement();
			stmt.execute("update item set quantity="+quantity+", enroll='"+df.format(date)+"' where id = "+id+";");
			stmt.close();
			conn.close();
			%>	<!-- 수정되었음을 알림 -->
			<script type="text/javascript">
			alert("상품이 수정되었습니다.");
			location.href="item_list.jsp";
			</script>
		<%
		}else{
			%>	<!-- 에러발생했을을 알림-->
			<script type="text/javascript">
			alert("에러가 발생했습니다. 다시 시도해 주시길 바랍니다.");
			window.history.back();
			</script>
			<%
		}	
%>
</body>
</html>