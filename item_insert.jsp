<meta http-equiv="content-Type" content="text/html; charset=utf-8" />
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import = "java.sql.*,javax.sql.*,java.io.*,java.util.*,java.text.*" %>
<html>
<head>
<title>KSW01_재고관리</title>
<!-- mystyle.css파일 import하여 적용-->
<link rel="stylesheet" type="text/css" href="mystyle.css">
<script>
<!-- 특수문자 입력을 막기 위한 함수 -->
function checkNumber(){
	var objEv = event.srcElement;
	var num ="{}[]()<>?_|~`!@#$%^&*-+\"'\\/";    //입력을 막을 특수문자 기재.
	event.returnValue = true;
	
	for (var i=0;i<objEv.value.length;i++){
		if(-1 != num.indexOf(objEv.value.charAt(i))){
			event.returnValue = false;
		}
	}
	if (!event.returnValue){
		alert("특수문자는 입력하실 수 없습니다.");
		var a="";
		for (var i=0;i<objEv.value.length-1;i++){
			a+=objEv.value.charAt(i);
		}
	objEv.value=a;
	}
}

</script>
<%	// 현재 시간을 가져오기 위해
	TimeZone tz;
	java.util.Date date = new java.util.Date();
	DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
	tz = TimeZone.getTimeZone("Asia/Seoul"); df.setTimeZone(tz);
	
%>
</head>
<body>
<center>	
<%	//객체 생성
	Class.forName("com.mysql.jdbc.Driver");
	// DB연결
	Connection conn=DriverManager.getConnection("jdbc:mysql://localhost/kopoctc","root","1234");
	//  테이블의 등록된 재고 수를 확인하기 위한 sql문
	Statement stmt=conn.createStatement();
	ResultSet rs=stmt.executeQuery("select count(id) from item;");
	int rows=0;
	while(rs.next()){
		rows=rs.getInt(1);
	}
	%>
	
	<table id="top" cellspacing="0">
		<tr>
			<td><h1>(주)트와이스 재고 현황 - 상품등록</h1></td>
		</tr>
	</table>
	<form method="post" action="item_write.jsp" enctype="multipart/form-data">
	<!--사진파일도 같이 등록해야 하기 때문에 enctype="multipart/form-data"로 설정-->
	<table border="1" cellspacing="0">
		<tr>
			<input type="hidden" name="info" value="INSERT" >
			<td width="10%">상품번호</td>
			<td width="90%" ><input type="number" name="id" min="1" max="100000" required></td>
		</tr>
		<tr>
			<td>상품명</td>
			<td><input type="text" name="name" onKeyDown="checkNumber();" pattern="^[\S]+" required maxlength="70" placeholder="70자 이내로 작성하시오.(특수문자 제외)"></td>
		</tr>
		<tr>
			<td width="10%">재고현황</td>
			<td width="90%" ><input type="number" name="quantity" min="0" max="100000" required></td>
		</tr>
		<tr>
			<td>상품등록일</td>
			<td><%=df.format(date)%></script></td>
		</tr>
			<td>재고등록일</td>
			<td><%=df.format(date)%></td>
		</tr>
		<tr>
			<td>상품설명</td>
			<td><textarea name="comment" onKeyDown="checkNumber();" required maxlength="1000"></textarea></td>	
		</tr>
		<tr>
			<td>상품사진</td>
			<td class="view" ><input type="file" name="file"></td>	
		</tr>
	</table>
	<table cellspacing="0">
		<tr>
			<td>
			<input type="submit" value="완료">
			<input type="button" value="취소" onclick="location.href='item_list.jsp'">
			</td>
		</tr>
	</table>
	</form>
</center>
</body>
</html>