<meta http-equiv="content-Type" content="text/html; charset=utf-8" />
<%@ page contentType="text/html; charset=utf-8" %>
<%@ page import = "java.sql.*,javax.sql.*,java.io.*,java.util.*,java.text.*" %>
<html>
<head>
<title>KSW01_재고관리</title>
<!-- mystyle.css파일 import하여 적용-->
<link rel="stylesheet" type="text/css" href="mystyle.css">
<!--전역변수 선언-->
<%! int id; %>
<%! String name; %>
<%! String photo; %>
<%	//한글깨짐 방지
	request.setCharacterEncoding("UTF-8");
	// 해당 id의 데이터를 조회하기 위한 파라미터
	String ids = request.getParameter("id");
	id=Integer.parseInt(ids);
%>
</head>
<body>
<center>
<%	// 객체 생성
	Class.forName("com.mysql.jdbc.Driver");
	// DB연결
	Connection conn=DriverManager.getConnection("jdbc:mysql://localhost/kopoctc","root","1234");
	// stmt 해당 id의 데이터를 조회하기 위한 sql문
	Statement stmt=conn.createStatement();
	ResultSet rs=stmt.executeQuery("select * from item where id = " + id + ";");
	%>
	<table id="top" cellspacing="0">
			<tr>
				<td><h1>(주)트와이스 재고 현황 - 상품상세</h1></td>
			</tr>
	</table>
	<form>
	<table cellspacing="0" border="1">
	<%
		while(rs.next()){
			name=rs.getString(2);
			photo=rs.getString(7);
			%>
			<tr>
				<th width='10%'>상품 번호</th>
				<td class="view" width='90%' name="cnt"><%=rs.getInt(1)%></td>
			</tr>
			<tr>
				<th>상품명</th>
				<td class="view" name="name"><%=rs.getString(2)%></td>
			</tr>
			<tr>
				<th>재고 현황</th>
				<td class="view"><%=rs.getInt(3)%></td>
			</tr>
			<tr>
				<th>상품등록일</th>
				<td class="view"><%=rs.getDate(4)%></td>
			</tr>
			<tr>
				<th>재고등록일</th>
				<td class="view"><%=rs.getDate(5)%></td>
			</tr>
			<tr>
				<th>상품설명</th>
				<td class="view"><%=rs.getString(6)%></td>
			</tr>
			<tr>
				<th>상품사진</th>
				<%
				if(photo==null){
				%>	<!--사진의 없을 경우-->
					<td class="view">등록된 사진이 없습니다.</td>
				<%	
				}else{
				%>	<!--사진이 있을 경우-->
					<td><img src="<%=rs.getString(7)%>" width="300" height="300"></td>
				<%	
				}
				%>
			</tr>
			<%
		}
	%>
	</table>
	<table cellspacing="0">
	<tr>
		<td>
		<input type="button" value="재고수정" onclick="window.location.href='item_update.jsp?id=<%=id%>'">
		<%
		if(photo==null){
		%>	<!--사진이 없을 경우 사진 값을 넘기지 않음-->
			<input type="button" value="상품삭제" onclick="window.location.href='item_delete.jsp?id=<%=id%>&name=<%=name%>'">
		<%
		}else{
		%>	<!--사진이 있을 경우 사진 값을 넘김-->
			<input type="button" value="상품삭제" onclick="window.location.href='item_delete.jsp?id=<%=id%>&name=<%=name%>&photo=<%=photo%>'">
		<%
		}	
		%>
		<input type="button" value="목록" onclick="window.location.href='item_list.jsp'">
		</td>
	</tr>
	</table>
	</form>
	<%	// 닫기
	rs.close();
	stmt.close();
	conn.close();
	%>
	</center>
</body>
</html>