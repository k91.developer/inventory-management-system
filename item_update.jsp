<meta http-equiv="content-Type" content="text/html; charset=utf-8" />
<%@ page contentType="text/html; charset=utf-8" %>
<%@ page import = "java.sql.*,javax.sql.*,java.io.*,java.util.*,java.text.*" %>
<html>
<head>
<title>KSW01_재고관리</title>
<!-- mystyle.css파일 import하여 적용-->
<link rel="stylesheet" type="text/css" href="mystyle.css">
<%	// 한글 깨짐을 방지하기 위한
	request.setCharacterEncoding("UTF-8");
	// 해당 id의 데이터를 가져오기 위한 파라미터
	int id;
	String ids = request.getParameter("id");
	id=Integer.parseInt(ids);
%>
<%	// 현재시간을 가져오기 위한
	TimeZone tz;
	java.util.Date date = new java.util.Date();
	DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
	tz = TimeZone.getTimeZone("Asia/Seoul"); df.setTimeZone(tz);
	//출력 : df.format(date)
%>
</head>
<body>
<center>
<%	// 객체 생성
	Class.forName("com.mysql.jdbc.Driver");
	// DB연결
	Connection conn=DriverManager.getConnection("jdbc:mysql://localhost/kopoctc","root","1234");
	// stmt 해당id의 데이터 조회하기 위한 sql문
	Statement stmt=conn.createStatement();
	ResultSet rs=stmt.executeQuery("select * from item where id = " + id + ";");
%>

<table id="top" cellspacing="0">
	<tr>
		<td><h1>(주)트와이스 재고 현황 - 재고수정</h1></td>
	</tr>
</table>
<!-- 사진도 전송하기 위하여 enctype="multipart/form-data"로 설정-->
<form method="post" action="item_write.jsp" enctype="multipart/form-data">
<table cellspacing="0" border="1">
	<%
	while(rs.next()){
		%>
		<tr>
			<th>상품번호</th>
			<td class="view"><%=rs.getInt(1)%></input></td>
			<input type="hidden" name="info" value="UPDATE">
			<input type="hidden" name="id" value="<%=rs.getInt(1)%>">
		</tr><tr>
			<th>상품명</th>
			<td class="view"><%=rs.getString(2)%></input></td>
		</tr><tr>
			<th>재고현황</th>
			<td width='90%'><input type=number name=quantity min="0" max="100000" value="<%=rs.getInt(3)%>"></td>
		</tr><tr>	
			<th>상품등록일</th>
			<td><%=rs.getDate(4)%></td>
		</tr><tr>
			<th>재고등록일</th>
			<td><%=df.format(date)%></td>
		</tr><tr>
			<th>상품설명</th>
			<td class="view"><%=rs.getString(6)%></td>
		</tr><tr>
			<th>상품사진</th>
			<%
			if(rs.getString(7)==null){
			%>	<!--사진이 등록되지 않았을 경우-->
				<td class="view">등록된 사진이 없습니다.</td>
			<%	
			}else{
			%>	<!--사진이 등록되었을 경우-->
				<td><img src="<%=rs.getString(7)%>"></td>
			<%	
			}
			%>
		</tr>
		<%
	}
	%>
</table>
<table cellspacing="0">
<tr>
	<td colspan=2 id="end">
		<input type="submit" value="재고수정">
		<input type="button" class="button" value="취소" onclick="window.location.href='item_list.jsp'">
	</td>
</tr>
</table>
</form>

<%	// 닫기
	rs.close();
	stmt.close();
	conn.close();
%>
</center>
</body>
</html>